import javafx.util.Pair;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Scanner;

public class Testing {
    public int methodTestOne(String hi){
        String hello = hi;
        if(hello.equals("greetings")){
            System.out.println("greetings to you too");
        } else if(hello.equals("salutations")){
            System.out.println("salutations to you too");
        } else {
            System.out.println("hello");
        }
        return 0;
    }

    public void fileToCSV(String filename){
        Scanner in = null;
        try {
            in = new Scanner(new File(filename));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        PrintWriter writer = null;
        try {
            writer = new PrintWriter("./" + filename + ".csv", "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        ArrayList<Pair<Pair<String,String>, Integer>> edges = new ArrayList<Pair<Pair<String,String>, Integer>>();
//        Stack<String> theStack = new Stack<String>();
        ArrayList<ArrayList<String>> theNodes = new ArrayList<>();
        int nodeCount = 0;
        int edgeCount = 0;
        // COUNT THE NODES THE FIRST TIME THROUGH THE FILE
        nodeCount = theGrandCounter(in, edges, theNodes, nodeCount, edgeCount);
        System.out.println("NC = " + nodeCount);
        System.out.println("EC = " + edgeCount);
        // CONSTRUCT THE MATRIX WITH THE PROPER DIMENSIONS
        String [][] theMatrix = new String[edgeCount][nodeCount];
        for(int i = 0; i < edgeCount; i++){
            for(int j = 0; j < nodeCount; j++){
                if(i == 0 && j == 0){
                    
                }
            }
        }


        // OPEN THE FILE AGAIN
        in.close();
        in = null;
        try {
            in = new Scanner(new File(filename));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        while(in.hasNext()){
            String temp = null;
            temp = in.nextLine();
            if(temp.contains("public") || temp.contains("private") || temp.contains("protected")){
                // CALL GRAND PARSER

                //nodeCount = this.theGrandParser(in,edges,theNodes,nodeCount,edgeCount);
                // OUTPUT TO THE FILE
//                writer.print("Nodes/Edges");
                System.out.print("Nodes/Edges");
                for(int i = 0; i < edges.size(); i++){
//                    writer.print(";" + Integer.toString(i));
                    System.out.print(";" + Integer.toString(i));
                }
//                writer.println("");
                System.out.println();
//                for(int i = 0; i < theNodes.size(); i++){
//                    writer.print(theNodes.get(i).get(0));
//                    for(int j = 1; j < edgeCount; j++){
//                        writer.print(";" + theNodes.get(i).get(j));
//                    }
//                    writer.println("");
//                }
                for(int i = 0; i < edges.size(); i++){
                    System.out.println("<" + edges.get(i).getKey().getKey() + "," + edges.get(i).getKey().getValue() + ">, " + edges.get(i).getValue().toString());
                }
            } else {
                System.err.println("ERROR: DID NOT GO TO GRAND PARSER");
            }
        }
    }

    public int theGrandParser(Scanner in, ArrayList<Pair<Pair<String,String>, Integer>> thyEdges, ArrayList<ArrayList<String>> thyNodes, int nC, int eC){
        boolean sameAct = false;
        int oGNC = nC;
        while(in.hasNext()){
            String temp = in.nextLine();
            if(temp.contains("}") && (!temp.contains("else"))){
                // END OF RECURSION LEVEL
                System.out.println("END OF RECURSION LEVEL" + " | TEMP = " + temp);
                break;
            } else if(temp.contains("if") && (!temp.contains("else"))){
                // NEW RECURSION LEVEL
                System.out.println("NEW RECURSION LEVEL" + " | TEMP = " + temp);
                // CALL GRAND PARSER
                nC++;
                System.out.println("      NC WAS " + (nC-1) +" when TEMP = " + temp);
                nC = theGrandParser(in,thyEdges,thyNodes,nC,eC);
            } else if(temp.contains("else")){
                // ASSUMING ANOTHER PART OF ORIGINAL DECISION
                // 'ELSE IF' OR 'ELSE'
                System.out.println("ANOTHER PART OF ORIGINAL DECISION" + " | TEMP = " + temp);
//                eC++;
                nC = theGrandParser(in,thyEdges,thyNodes,nC,eC);
            } else {
                // ASSUME ACTIVITY
                if(!sameAct){
                    // NEW ACTIVITY
                    System.out.println("NEW ACTIVITY" + " | TEMP = " + temp);
                    sameAct = true;
                    nC++;
                    System.out.println("      NC WAS " + (nC-1) +" when TEMP = " + temp);
                    Pair<String,String> temp1 = new Pair<String,String>(String.valueOf((char)(oGNC + 'A')),String.valueOf((char)(nC + 'A')));
                    eC++;
                    Pair<Pair<String,String>, Integer> addMe = new Pair<Pair<String,String>, Integer>(temp1, eC);
                    thyEdges.add(addMe);
                } else {
                    System.out.println("SAME ACTIVITY" + " | TEMP = " + temp);
                }
            }
        }
        return nC;
    }

    public int theGrandCounter(Scanner in, ArrayList<Pair<Pair<String,String>, Integer>> thyEdges, ArrayList<ArrayList<String>> thyNodes, int nC, int eC){
        boolean sameAct = false;
//        int oGNC = nC;
        while(in.hasNext()){
            String temp = in.nextLine();
            if(temp.contains("}") && (!temp.contains("else"))){
                // END OF RECURSION LEVEL
//                System.out.println("END OF RECURSION LEVEL" + " | TEMP = " + temp);
                break;
            } else if(temp.contains("if") && (!temp.contains("else"))){
                // NEW RECURSION LEVEL
//                System.out.println("NEW RECURSION LEVEL" + " | TEMP = " + temp);
                // CALL GRAND PARSER
                nC++;
                System.out.println("      NC WAS " + (nC-1) +" when TEMP = " + temp);
                nC = theGrandCounter(in,thyEdges,thyNodes,nC,eC);
            } else if(temp.contains("else")){
                // ASSUMING ANOTHER PART OF ORIGINAL DECISION
                // 'ELSE IF' OR 'ELSE'
//                System.out.println("ANOTHER PART OF ORIGINAL DECISION" + " | TEMP = " + temp);
//                eC++;
                nC = theGrandCounter(in,thyEdges,thyNodes,nC,eC);
            } else {
                // ASSUME ACTIVITY
                if(!sameAct){
                    // NEW ACTIVITY
//                    System.out.println("NEW ACTIVITY" + " | TEMP = " + temp);
//                    sameAct = true;
                    nC++;
                    System.out.println("      NC WAS " + (nC-1) +" when TEMP = " + temp);
//                    Pair<String,String> temp1 = new Pair<String,String>(String.valueOf((char)(oGNC + 'A')),String.valueOf((char)(nC + 'A')));
                    eC++;
//                    Pair<Pair<String,String>, Integer> addMe = new Pair<Pair<String,String>, Integer>(temp1, eC);
//                    thyEdges.add(addMe);
                } else {
//                    System.out.println("SAME ACTIVITY" + " | TEMP = " + temp);
                }
            }
        }
        return nC;
    }

    public void convertToOxygen(String filename){
        Scanner in = null;
        try {
            in = new Scanner(new File(filename));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(filename + ".csv", "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        ArrayList<Pair<String,String>> thyEdges = new ArrayList<>();
        while(in.hasNext()){
            String temp = null;
            temp = in.nextLine();
            if(temp.contains("->")){
                String [] temp2 = temp.split(" ");
                thyEdges.add(new Pair<String,String>(temp2[0],temp2[2]));
                //System.out.println(temp2[0] + " IS PARENT OF " + temp2[2]);
            }
        }
        System.out.print("Nodes/Edges");
        assert writer != null;
        writer.print("Nodes/Edges");
        for(int i = 0; i < thyEdges.size(); i++){
            System.out.print(";" + (char)(Integer.valueOf(i) + 'A'));
            writer.print(";" + (char)(Integer.valueOf(i) + 'A'));
        }
        System.out.println();
        writer.println();

        for(int i = 0; i < thyEdges.size(); i++){
            //System.out.println("EDGE #" + i + ": " + thyEdges.get(i).getKey() + " IS PARENT OF " + thyEdges.get(i).getValue());
//            System.out.print(thyEdges.get(i).getKey());
            try {
                System.out.print((Integer.valueOf(thyEdges.get(i).getKey())));
                writer.print((Integer.valueOf(thyEdges.get(i).getKey())));
//                Integer.parseInt(thyEdges.get(i).getKey());
//                System.out.print((char)(i + 'A'));
//                writer.print((char)(i + 'A'));
            } catch (Exception e){
                System.out.print(thyEdges.get(i).getKey());
                writer.print(thyEdges.get(i).getKey());
//                System.out.print(thyEdges.get(i).getKey());
//                writer.print(thyEdges.get(i).getKey());
            }

//            String.valueOf(thyEdges.get(i).getKey()) + 'A');
//            writer.print(thyEdges.get(i).getKey());
            for(int j = 0; j < thyEdges.size(); j++){
                if(i == j){
                    System.out.print(";" + thyEdges.get(i).getValue());
                    writer.print(";" + thyEdges.get(i).getValue());

                } else {
                    System.out.print(";-");
                    writer.print(";-");
                }
            }
            System.out.println();
            writer.println();
        }
        writer.close();
    }
}