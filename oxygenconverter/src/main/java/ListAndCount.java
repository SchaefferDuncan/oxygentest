import javafx.util.Pair;
import java.util.ArrayList;

public class ListAndCount {
    public ArrayList<Pair<String,String>> edges;
    public int numOfNodes;

    public ListAndCount(ArrayList<Pair<String,String>> e, int n){
        this.edges = e;
        this.numOfNodes = n;
    }

    public ArrayList<Pair<String, String>> getEdges() {
        return edges;
    }

    public void setEdges(ArrayList<Pair<String, String>> edges) {
        this.edges = edges;
    }

    public int getNumOfNodes() {
        return numOfNodes;
    }

    public void setNumOfNodes(int numOfNodes) {
        this.numOfNodes = numOfNodes;
    }
}
