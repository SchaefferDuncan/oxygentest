import javafx.util.Pair;
import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try {
            if(!args[0].contains(".dot") && !args[0].contains(".java")){
                System.err.println("ERROR: Invalid file format passed");
                System.err.println("Only <filename>.dot OR <filename>.java are compatible!");
            } else {
                // INITIALIZE THE READER AND WRITER ALONG WITH THE LIST OF EDGES
                Scanner in = null;
                PrintWriter writer = null;
                int methodCount = 0;
                ArrayList<Pair<String,String>> thyEdges = new ArrayList<Pair<String,String>>();
                try {
                    in = new Scanner(new File(args[0]));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // STRIP THE FILE EXTENSION AND INITIALIZE WRITER
                String simpleFileName = args[0].split("[.]")[0];

                // BRANCH OFF TO ANALYZE FILE DEPENDING ON FORMAT
                if(args[0].contains(".dot")){
                    try {
                        writer = new PrintWriter(simpleFileName + ".csv", "UTF-8");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    convertDotToOxygen(in, writer, thyEdges);
                    writer.close();
                } else {
                    while(in.hasNext()){
                        String tempL = in.nextLine();
                        if(tempL.contains("{")){
                            try {
                                writer = new PrintWriter(simpleFileName + methodCount + ".csv", "UTF-8");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            convertJavaToOxygen(in, writer, thyEdges);
                            methodCount++;
                            writer.close();
                        }
                    }

                }

                // OUTPUT OF THE EDGES WITH AUTO-INCREMENTING STRING
                System.out.print("Nodes/Edges");
                assert writer != null;
                writer.print("Nodes/Edges");
                String prefixString = "";
                for(int i = 0; i < thyEdges.size(); i++){
                    if(i % 26 == 0 && i != 0){
                        prefixString += "A";
                    }
                    System.out.print(";" + prefixString + (char)((i % 26) + 'A'));
                    writer.print(";" + prefixString + (char)((i % 26) + 'A'));
                }
                System.out.println();
                writer.println();

                // OUTPUT OF THE NODES AND THEIR CHILDREN
                for(int i = 0; i < thyEdges.size(); i++){
                    try {
                        System.out.print((Integer.valueOf(thyEdges.get(i).getKey())));
                        writer.print((Integer.valueOf(thyEdges.get(i).getKey())));
                    } catch (Exception e){
                        System.out.print(thyEdges.get(i).getKey());
                        writer.print(thyEdges.get(i).getKey());
                    }

                    for(int j = 0; j < thyEdges.size(); j++){
                        if(i == j){
                            System.out.print(";" + thyEdges.get(i).getValue());
                            writer.print(";" + thyEdges.get(i).getValue());
                        } else {
                            System.out.print(";-");
                            writer.print(";-");
                        }
                    }
                    System.out.println();
                    writer.println();
                }

                // CLOSE OFF THE OUTPUT FILE
                writer.close();
            }
        } catch(Exception e){
            System.err.println("ERROR: Invalid parameters passed!");
        }
    }

    public static void convertDotToOxygen(Scanner fileIn, PrintWriter fileOut, ArrayList<Pair<String,String>> edgeList){
        // PARSE AND CORRECTLY READ IN DATA
        while(fileIn.hasNext()){
            String temp = fileIn.nextLine();
            if(temp.contains("->")){
                String [] temp2 = temp.split(" ");
                edgeList.add(new Pair<String,String>(temp2[0],temp2[2]));
            }
        }
    }

    public static void convertJavaToOxygen(Scanner fileIn, PrintWriter fileOut, ArrayList<Pair<String,String>> edgeList){
        // PARSE AND CORRECTLY READ IN DATA
        int nodeCount = 0;
        if(fileIn.hasNext()){
            ListAndCount tLaC = levelAnalysis(fileIn, fileOut, edgeList, nodeCount);
        } else {
            // ASSUMING THERE WAS NOTHING IN THE FILE
            System.err.println("ERROR: File was empty!");
        }
        edgeList.add(new Pair<String, String>("start", "0"));
    }

    public static ListAndCount levelAnalysis(Scanner fileIn, PrintWriter fileOut, ArrayList<Pair<String,String>> edgeList, int nodeCount){
        int pNC = nodeCount;
        boolean sameActivity = false;
        while(fileIn.hasNext()) {
            String temp = fileIn.nextLine();
            if (temp.contains("}") && (!temp.contains("else"))) {
                sameActivity = false;
                // ASSUMING END OF ONE LEVEL OR CHILDREN NODE
                System.out.println("END OF RECURSION LEVEL" + " | TEMP = " + temp);
                nodeCount--;
                break;
            } else if (temp.contains("if") && !temp.contains("else")) {
                sameActivity = false;
                // ASSUMING BEGINNING OF LEVEL OR BRACKET
                System.out.println("NEW RECURSION LEVEL" + " | TEMP = " + temp);
                System.out.println("      NC WAS " + (nodeCount-1) +" when TEMP = " + temp);
                // ADD EDGE TO THE LIST
                nodeCount++;
                Pair<String, String> tPair = new Pair<String, String>(Integer.toString(pNC),Integer.toString(nodeCount));
                edgeList.add(tPair);
                // GO A LEVEL DEEP(ER)
                ListAndCount tLaC = levelAnalysis(fileIn, fileOut, edgeList, nodeCount);
                edgeList = tLaC.getEdges();
                nodeCount = tLaC.getNumOfNodes();
            } else if (temp.contains("else")) {
                sameActivity = false;
                // ASSUMING ANOTHER PART OF ORIGINAL DECISION ('ELSE IF' OR 'ELSE')
                System.out.println("ANOTHER PART OF ORIGINAL DECISION" + " | TEMP = " + temp);
                ListAndCount tLaC = levelAnalysis(fileIn, fileOut, edgeList, nodeCount);
                edgeList = tLaC.getEdges();
                nodeCount = tLaC.getNumOfNodes();
            } else {
                // ASSUMING ACTIVITY
                if (!sameActivity) {
                    // ASSUMING NEW ACTIVITY
                    System.out.println("NEW ACTIVITY" + " | TEMP = " + temp);
                    sameActivity = true;
                    nodeCount++;
                    System.out.println("      NC WAS " + (nodeCount-1) +" when TEMP = " + temp);
                    // ADD EDGE TO THE LIST
                    Pair<String, String> tPair = new Pair<String, String>(Integer.toString(pNC),Integer.toString(nodeCount));
                    edgeList.add(tPair);
                } else {
                    // ASSUMING THE SAME ACTIVITY
                    System.out.println("SAME ACTIVITY" + " | TEMP = " + temp);
                }
            }
        }
        return new ListAndCount(edgeList, nodeCount);
    }
}
